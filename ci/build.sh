#!/usr/bin/env bash

go fmt ./src/...

#go build ./src/app/cmd/app_main.go
go install ./src/app/cmd/app_main.go

# to debug
# dlv debug ./src/app/cmd/app_main.go