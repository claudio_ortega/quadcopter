package uarts

import (
	"fmt"
	"github.com/hashicorp/go-multierror"
	"github.com/ian-kent/go-log/log"
	"github.com/ian-kent/go-log/logger"
	"github.com/tarm/serial"
	"time"
)

type Uart struct {
	port *serial.Port
	log  logger.Logger
}

func CreateInstance(pathName string, bitRate int) (*Uart, error) {

	if pathName == "" {
		return nil, fmt.Errorf("pathname for serial port is empty")
	} else {

		tmpLog := log.Logger("uart")

		// serial port
		serialConfig := serial.Config{
			Name:        pathName,
			Baud:        bitRate,
			Size:        8,
			Parity:      serial.ParityNone,
			StopBits:    serial.Stop1,
			ReadTimeout: 500 * time.Millisecond,
		}

		tmpLog.Info("attempting UART creation with config: [%s @ %d,%d,%c,%d]",
			serialConfig.Name,
			serialConfig.Baud,
			serialConfig.Size,
			rune(serialConfig.Parity),
			serialConfig.StopBits,
		)

		serialPort, err := serial.OpenPort(&serialConfig)

		if err != nil {
			return nil, err
		}

		return &Uart{
			port: serialPort,
			log:  tmpLog,
		}, nil
	}
}

func (u *Uart) ConsumeAndLogForLoopBack() {

	totalBytesRcvd := 0
	rxBuffer := make([]byte, 32)

	for {
		bytesReceived, err := u.port.Read(rxBuffer)
		totalBytesRcvd += bytesReceived

		if err != nil {
			u.log.Debug("%v", err)
		} else {
			if bytesReceived > 0 {
				u.log.Debug("received:[%d], total:[%d], content:[0x%x]", bytesReceived, totalBytesRcvd, rxBuffer[0])
			} else {
				u.log.Debug("no bytes received")
			}
		}
	}
}

func (u *Uart) ConsumeAndLogForGPS() {

	rxBuffer := make([]byte, 200)

	for {
		bytesReceived, err := u.port.Read(rxBuffer)

		if err != nil {
			u.log.Debug("%v", err)
		} else {
			for i := 0; i < bytesReceived; i++ {
				u.log.Debug("%40s", string(rxBuffer))
			}
		}
	}
}

func (u *Uart) Write(buffer []byte) (n int, err error) {
	return u.port.Write(buffer)
}

func (u *Uart) Close() error {
	var result *multierror.Error
	err1 := u.port.Flush()
	result = multierror.Append(result, err1)
	err2 := u.port.Close()
	result = multierror.Append(result, err2)
	return result.ErrorOrNil()
}
