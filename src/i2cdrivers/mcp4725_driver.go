package i2cdrivers

import (
	"fmt"
	"github.com/ian-kent/go-log/log"
	"github.com/ian-kent/go-log/logger"
	"gobot.io/x/gobot"
	"gobot.io/x/gobot/drivers/i2c"
)

type MCP4725Driver struct {
	name       string
	connector  i2c.Connector
	connection i2c.Connection
	logger     logger.Logger
	i2c.Config
	gobot.Commander
}

// NewSI5351Driver creates a new driver
// xtalFreqHz should match the value in the adafruit breakout
// see https://www.adafruit.com/product/2045
func NewMCP4725Driver(connector i2c.Connector, config i2c.Config) *MCP4725Driver {
	return &MCP4725Driver{
		name:      gobot.DefaultName("mcp4725"),
		connector: connector,
		Config:    config,
		Commander: gobot.NewCommander(),
		logger:    log.Logger("MCP4725Driver"),
	}
}

func (p *MCP4725Driver) Name() string { return p.name }

func (p *MCP4725Driver) SetName(n string) { p.name = n }

func (p *MCP4725Driver) Connection() gobot.Connection { return p.connector.(gobot.Connection) }

func (p *MCP4725Driver) Start() error {

	// make sure the bus/address values were properly defined,
	// by purposely avoiding good defaults
	bus := p.Config.GetBusOrDefault(-1)
	address := p.GetAddressOrDefault(-1)

	var err error
	p.connection, err = p.connector.GetConnection(address, bus)

	return err
}

func (p *MCP4725Driver) Halt() error {
	return nil
}

// SetValue uses (see chip data sheet) the Fast Mode Write Command Mode ( {C2,C1}={0,0} )
// but sending just one value at a time,see Fig 6-1 in the data sheet
func (p *MCP4725Driver) SetValue(value int) error {

	if value < 0 || value > 4095 {
		return fmt.Errorf("wrong value: %d", value)
	}

	// the first byte is being sent before, here we are passing the 2nd and third bytes
	secondByte := byte((value >> 8) & 0xff)
	thirdByte := byte(value & 0xff)

	_, err := p.connection.Write([]byte{secondByte, thirdByte})

	return err
}
