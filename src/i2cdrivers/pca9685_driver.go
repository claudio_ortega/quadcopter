package i2cdrivers

import (
	"github.com/ian-kent/go-log/log"
	"github.com/ian-kent/go-log/logger"
	"gobot.io/x/gobot"
	"gobot.io/x/gobot/drivers/i2c"
)

const (
	PCA9685_MODE1_REGISTER            = byte(0x00)
	PCA9685_PRESCALE_REGISTER         = byte(0xFE)
	PCA9685_LED_BASE_CONTROL_REGISTER = byte(0x06)
	PCA9685_ALLLED_OFF_HIGH_REGISTER  = byte(0xFD)
)

// PCA9685Driver is for the PCA9685 16-channel 12-bit PWM/Servo controller.
// reference: https://www.adafruit.com/product/815
//
type PCA9685Driver struct {
	name       string
	connector  i2c.Connector
	connection i2c.Connection
	i2c.Config
	gobot.Commander
	logger logger.Logger
}

// NewPCA9685Driver creates a new driver
func NewPCA9685Driver(a i2c.Connector, config i2c.Config) *PCA9685Driver {
	return &PCA9685Driver{
		name:      gobot.DefaultName("pca9685"),
		connector: a,
		Config:    config,
		Commander: gobot.NewCommander(),
		logger:    log.Logger("PCA9685Driver"),
	}
}

// Name returns driver's name
func (p *PCA9685Driver) Name() string { return p.name }

// SetName sets driver's name
func (p *PCA9685Driver) SetName(n string) { p.name = n }

// Connection returns the i2c connection
func (p *PCA9685Driver) Connection() gobot.Connection { return p.connector.(gobot.Connection) }

// Start initializes the chip
func (p *PCA9685Driver) Start() error {

	// make sure the bus/address values were properly defined,
	// by purposely avoiding good defaults
	bus := p.Config.GetBusOrDefault(-1)
	address := p.GetAddressOrDefault(-1)

	var err error
	p.connection, err = p.connector.GetConnection(address, bus)

	if err != nil {
		return err
	}

	_, err = p.connection.Write([]byte{PCA9685_MODE1_REGISTER, 0x00})

	if err != nil {
		return err
	}

	// turns off all the pins, see table 7 in the data sheet
	_, err = p.connection.Write([]byte{PCA9685_ALLLED_OFF_HIGH_REGISTER, 0x10})
	if err != nil {
		return err
	}

	return nil
}

// Halt makes all outputs to go to zero
func (p *PCA9685Driver) Halt() error {
	_, err := p.connection.Write([]byte{PCA9685_ALLLED_OFF_HIGH_REGISTER, 0x10})
	return err
}

// SetPulseWidth sets a specific channel to a pwm value from 0-4096.
// start count is always set to zero, so then the off count is the desired count width
//
func (p *PCA9685Driver) SetPulseWidth(channel int, widthCount uint16) error {

	// this uses sequential write, ie: for channel==0
	// the addresses to be used are [06h, 07h, 08h, 09h]
	// see table 6 in data sheet
	_, err := p.connection.Write(
		[]byte{
			PCA9685_LED_BASE_CONTROL_REGISTER + byte(4*channel),
			0,
			0,
			byte(widthCount),
			byte(widthCount >> 8)})

	return err
}

// SetFrequency sets the frequency for all channels at the same time.
// This has to be called once before SetPulseWidth, and it is not needed anymore afterwards.
// See comment in method getScalingFactorAsByte().
func (p *PCA9685Driver) SetFrequency(freqInHz float32) error {

	_, err := p.connection.Write([]byte{PCA9685_MODE1_REGISTER})
	if err != nil {
		return err
	}

	readBuffer := []byte{PCA9685_MODE1_REGISTER, 0}
	_, err = p.connection.Read(readBuffer)
	if err != nil {
		return err
	}

	previousMode := readBuffer[1]

	// from the data sheet 7.3.5:
	// The PRE_SCALE register can only be set when the SLEEP bit of MODE1 register is set to logic 1.
	newMode := byte((previousMode & 0x7F) | 0x10)

	_, err = p.connection.Write([]byte{PCA9685_MODE1_REGISTER, newMode})
	if err != nil {
		return err
	}

	_, err = p.connection.Write([]byte{PCA9685_PRESCALE_REGISTER, p.getScalingFactorAsByte(freqInHz)})
	if err != nil {
		return err
	}

	// put old settings back on
	_, err = p.connection.Write([]byte{PCA9685_MODE1_REGISTER, previousMode})
	if err != nil {
		return err
	}

	// enableOutput auto-increment, clear restart
	_, err = p.connection.Write([]byte{PCA9685_MODE1_REGISTER, previousMode | 0xa1})
	if err != nil {
		return err
	}

	return nil
}

// getScalingFactorAsByte implements section 7.3.5 in the pca9685 data sheet
// The chip's internal oscillator frequency is 25 MHz (+/-10%), therefore,
// the passed in freq should be adjusted to the particular -chip instance-.
// Even worse, as the scaling factor will have to be one byte register value,
// then the desired frequency will seldom be able to get achieved,
// and you will have to live, even after any adjustment,
// with a rough approximate error in the freq of around 1-4 percent
// the smaller the factor value, the worse would be the rounding error
func (p *PCA9685Driver) getScalingFactorAsByte(freqInHz float32) byte {

	const (
		CLOCK_FREQ_HZ = float32(25000000)
		MAX_COUNTS    = int32(4096)
	)

	p.logger.Printf("requested freq, f:%v", freqInHz)
	scalingFactorAsFloat := (CLOCK_FREQ_HZ / (float32(MAX_COUNTS) * freqInHz)) + 0.5
	scalingFactorAsByte := byte(scalingFactorAsFloat) - 1
	p.logger.Printf("scalingFactor, f:%v, b:%v", scalingFactorAsFloat, scalingFactorAsByte)
	p.logger.Printf("effective freq, f:%v", CLOCK_FREQ_HZ/float32(MAX_COUNTS*int32(scalingFactorAsByte+1)))

	return scalingFactorAsByte
}
