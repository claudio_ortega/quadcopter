package i2cdrivers

import (
	"fmt"
	"github.com/ian-kent/go-log/log"
	"github.com/ian-kent/go-log/logger"
	"gobot.io/x/gobot"
	"gobot.io/x/gobot/drivers/i2c"
)

/*
	elements taken from
		https://github.com/pavelmc/Si5351mcu/blob/master/src/si5351mcu.h
		https://github.com/pavelmc/Si5351mcu/blob/master/src/si5351mcu.cpp
*/

type SI5351Driver struct {
	name       string
	connector  i2c.Connector
	connection i2c.Connection
	i2c.Config
	gobot.Commander
	xtalFreqHz int
	logger     logger.Logger
}

// NewSI5351Driver creates a new driver
// xtalFreqHz should match the value in the adafruit breakout
// see https://www.adafruit.com/product/2045
func NewSI5351Driver(connector i2c.Connector, config i2c.Config, inXtalFreqHz int) *SI5351Driver {
	return &SI5351Driver{
		name:       gobot.DefaultName("si5351"),
		connector:  connector,
		Config:     config,
		Commander:  gobot.NewCommander(),
		xtalFreqHz: inXtalFreqHz,
		logger:     log.Logger("SI5351Driver"),
	}
}

func (p *SI5351Driver) Name() string { return p.name }

func (p *SI5351Driver) SetName(n string) { p.name = n }

func (p *SI5351Driver) Connection() gobot.Connection { return p.connector.(gobot.Connection) }

func (p *SI5351Driver) Start() error {

	// make sure the bus/address values were properly defined,
	// by purposely avoiding good defaults
	bus := p.Config.GetBusOrDefault(-1)
	address := p.GetAddressOrDefault(-1)

	var err error
	p.connection, err = p.connector.GetConnection(address, bus)

	if err != nil {
		return err
	}

	p.init()

	return p.disableAllOutputs()
}

func (p *SI5351Driver) Halt() error {
	return p.disableAllOutputs()
}

// ForTestComputeRegisters, ForTest makes it obvious that is for testing purposes only
func (p *SI5351Driver) TComputeRegisters(clkIndex int, freq int) (
	R int,
	divider int,
	MSxP1 int,
	MSNxP1 int,
	MSNxP2 int,
	MSNxP3 int,
	err error,
) {
	return p.computeRegisters(clkIndex, freq)
}

//
func (p *SI5351Driver) computeRegisters(clkIndex int, freq int) (
	R int,
	divider int,
	MSxP1 int,
	MSNxP1 int,
	MSNxP2 int,
	MSNxP3 int,
	err error,
) {
	p.logger.Info("clkIndex:%d, freq:%d", clkIndex, freq)

	divider = 900000000 / freq
	R = 1

	for divider > 900 {
		R = R * 2
		divider = divider / 2
	}

	// finds the even divider which delivers the intended Frequency
	if divider%2 == 1 {
		divider--
	}

	p.logger.Debug("divider:%d, R:%d", divider, R)

	// Calculate the PLL-Frequency (given the even divider)
	vcoFreq := divider * R * freq

	a := vcoFreq / p.xtalFreqHz
	b := (vcoFreq % p.xtalFreqHz) >> 5 // Integer par of the fraction
	c := p.xtalFreqHz >> 5             // "c" scaled to match it's limits in the register
	f := (128 * b) / c                 // f is (128*b)/c to mimic the Floor(128*(b/c)) from the datasheet

	p.logger.Debug("vcoFreq:%d, a:%d, b:%d, c:%d, f:%d", divider, a, b, c, f)

	// See datasheet, special trick when R=4
	// Convert the Output Divider to the bit-setting required in register 44
	rTx := map[int]int{
		0x01: 0x00, 0x02: 0x10, 0x04: 0x20, 0x08: 0x30,
		0x10: 0x40, 0x20: 0x50, 0x40: 0x60, 0x80: 0x70,
	}

	p.logger.Debug("R(1):%x", R)
	R, _ = rTx[R]
	p.logger.Debug("R(2):%x", R)

	// we have now the integer part of the output msynth
	// the b & c is fixed below
	MSxP1 = 128*divider - 512
	MSNxP1 = 128*a + f - 512
	MSNxP2 = 128*b - f*c
	MSNxP3 = c

	p.logger.Debug("MSxP1:%x, MSNxP1:%x, MSNxP2:%x, MSNxP3:%x", MSxP1, MSNxP1, MSNxP2, MSNxP3)

	return R, divider, MSxP1, MSNxP1, MSNxP2, MSNxP3, nil
}

const (
	//SI5351_REGISTER_3   = 3
	SI5351_REGISTER_16  = 16
	SI5351_REGISTER_24  = 24
	SI5351_REGISTER_165 = 165
	SI5351_REGISTER_166 = 166
	SI5351_REGISTER_167 = 167
	SI5351_ENABLE_CLK   = 0x4c
	SI5351_DISABLE_CLK  = 0x80
)

func (p *SI5351Driver) init() {
	//p.i2cWriteNE(SI5351_REGISTER_3, 0)
	p.i2cWriteNE(SI5351_REGISTER_24, 0)
	p.i2cWriteNE(SI5351_REGISTER_165, 0)
	p.i2cWriteNE(SI5351_REGISTER_166, 0)
	p.i2cWriteNE(SI5351_REGISTER_167, 0)
}

func (p *SI5351Driver) disableOutput(clkIndex int) error {
	return p.i2cWrite(SI5351_REGISTER_16+clkIndex, SI5351_DISABLE_CLK)
}

func (p *SI5351Driver) enableOutput(clkIndex int) error {
	return p.i2cWrite(SI5351_REGISTER_16+clkIndex, SI5351_ENABLE_CLK)
}

func (p *SI5351Driver) SetFrequency(clkIndex int, freq int) error {

	R, divider, MSxP1, MSNxP1, MSNxP2, MSNxP3, err := p.computeRegisters(clkIndex, freq)

	const (
		SI5351_REGISTER_26  = 26
		SI5351_REGISTER_27  = 27
		SI5351_REGISTER_28  = 28
		SI5351_REGISTER_29  = 29
		SI5351_REGISTER_30  = 30
		SI5351_REGISTER_31  = 31
		SI5351_REGISTER_32  = 32
		SI5351_REGISTER_33  = 33
		SI5351_REGISTER_42  = 42
		SI5351_REGISTER_43  = 43
		SI5351_REGISTER_44  = 44
		SI5351_REGISTER_45  = 45
		SI5351_REGISTER_46  = 46
		SI5351_REGISTER_47  = 47
		SI5351_REGISTER_48  = 48
		SI5351_REGISTER_49  = 49
		SI5351_REGISTER_177 = 177
	)

	{
		// PLLs
		pllOffset := 0
		p.i2cWriteNE(SI5351_REGISTER_26+pllOffset, (MSNxP3&0xff00)>>8) // Bits [15:8] of MSNxP3 in register 26
		p.i2cWriteNE(SI5351_REGISTER_27+pllOffset, MSNxP3&0xff)
		p.i2cWriteNE(SI5351_REGISTER_28+pllOffset, (MSNxP1&0x30000)>>16)
		p.i2cWriteNE(SI5351_REGISTER_29+pllOffset, (MSNxP1&0xff00)>>8)                            // Bits [15:8]  of MSNxP1 in register 29
		p.i2cWriteNE(SI5351_REGISTER_30+pllOffset, MSNxP1&0xff)                                   // Bits [7:0]  of MSNxP1 in register 30
		p.i2cWriteNE(SI5351_REGISTER_31+pllOffset, ((MSNxP3&0xf0000)>>12)|((MSNxP2&0xf0000)>>16)) // Parts of MSNxP3 and MSNxP1
		p.i2cWriteNE(SI5351_REGISTER_32+pllOffset, (MSNxP2&0xff00)>>8)                            // Bits [15:8]  of MSNxP2 in register 32
		p.i2cWriteNE(SI5351_REGISTER_33+pllOffset, MSNxP2&0xff)                                   // Bits [7:0]  of MSNxP2 in register 33
	}

	{
		// CLK# registers are 8 * clkIndex bytes shifted from each base register.
		registerOffset := clkIndex * 8

		// multisynths
		p.i2cWriteNE(SI5351_REGISTER_42+registerOffset, 0) // Bits [15:8] of MS0_P3 (always 0)
		p.i2cWriteNE(SI5351_REGISTER_43+registerOffset, 1) // Bits [7:0]  of MS0_P3 (always 1)

		if divider == 4 {
			p.i2cWriteNE(SI5351_REGISTER_44+registerOffset, 0x0c|R)
			p.i2cWriteNE(SI5351_REGISTER_45+registerOffset, 0) // Bits [15:8] of MSxP1 must be 0
			p.i2cWriteNE(SI5351_REGISTER_46+registerOffset, 0) // Bits [7:0] of MSxP1 must be 0
		} else {
			p.i2cWriteNE(SI5351_REGISTER_44+registerOffset, ((MSxP1&0x30000)>>16)|R) // Bits [17:16] of MSxP1 in bits [1:0] and R in [7:4]
			p.i2cWriteNE(SI5351_REGISTER_45+registerOffset, (MSxP1&0xff00)>>8)       // Bits [15:8]  of MSxP1 in register 45
			p.i2cWriteNE(SI5351_REGISTER_46+registerOffset, MSxP1&0xff)              // Bits [7:0]  of MSxP1 in register 46
		}

		p.i2cWriteNE(SI5351_REGISTER_47+registerOffset, 0) // Bits [19:16] of MS0_P2 and MS0_P3 are always 0
		p.i2cWriteNE(SI5351_REGISTER_48+registerOffset, 0) // Bits [15:8]  of MS0_P2 are always 0
		p.i2cWriteNE(SI5351_REGISTER_49+registerOffset, 0) // Bits [7:0]   of MS0_P2 are always 0
	}

	// reset the pll
	p.i2cWriteNE(SI5351_REGISTER_177, 0xa0)

	err = p.enableOutput(clkIndex)

	return err
}

func (p *SI5351Driver) disableAllOutputs() error {
	for i := 0; i < 2; i++ {
		err := p.disableOutput(i)
		if err != nil {
			return err
		}
	}

	return nil //p.i2cWrite(SI5351_REGISTER_3, 0xff)
}

func (p *SI5351Driver) i2cWrite(register int, value int) error {

	if register < 0x00 || register > 0xff {
		return fmt.Errorf("wrong register: %d", register)
	}

	if value < 0x00 || value > 0xff {
		return fmt.Errorf("wrong value: %d", value)
	}

	_, err := p.connection.Write([]byte{byte(register), byte(value)})

	return err
}

func (p *SI5351Driver) i2cWriteNE(register int, value int) {
	err := p.i2cWrite(register, value)
	if err != nil {
		p.logger.Error("error: %v", err)
	}
}
