package experimental

import (
	gocheck "gopkg.in/check.v1"
	"testing"
)

func TestSuiteBlahA(t *testing.T) {
	t.Logf("TestSuiteBlahA() -- START")
	gocheck.Suite(&SuiteBlahA{})
	gocheck.TestingT(t)
	t.Logf("TestSuiteBlahA() -- END")
}

type SuiteBlahA struct{}

func (s *SuiteBlahA) SetUpSuite(c *gocheck.C) {
	c.Logf("SuiteBlahA.SetUpSuite() -- START")
	c.Logf("SuiteBlahA.SetUpSuite() -- END")
}

func (s *SuiteBlahA) TearDownSuite(c *gocheck.C) {
	c.Logf("SuiteBlahA.TearDownSuite() -- START")
	c.Logf("SuiteBlahA.TearDownSuite() -- END")
}

func (s *SuiteBlahA) SetUpTest(c *gocheck.C) {
	c.Logf("SuiteBlahA.SetUpTest() -- START")
	c.Logf("SuiteBlahA.SetUpTest() -- END")
}

func (s *SuiteBlahA) TearDownTest(c *gocheck.C) {
	c.Logf("SuiteBlahA.TearDownTest() -- START")
	c.Logf("SuiteBlahA.TearDownTest() -- END")
}
