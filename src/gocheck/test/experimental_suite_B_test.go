package experimental

import (
	gocheck "gopkg.in/check.v1"
	"testing"
)

func TestSuiteBlahB(t *testing.T) {
	t.Logf("TestSuiteBlahB() -- START")
	gocheck.Suite(&SuiteBlahB{})
	gocheck.TestingT(t)
	t.Logf("TestSuiteBlahB() -- END")
}

type SuiteBlahB struct{}

func (s *SuiteBlahB) SetUpSuite(c *gocheck.C) {
	c.Logf("SuiteBlahB.SetUpSuite() -- START")
	c.Logf("SuiteBlahB.SetUpSuite() -- END")
}

func (s *SuiteBlahB) TearDownSuite(c *gocheck.C) {
	c.Logf("SuiteBlahB.TearDownSuite() -- START")
	c.Logf("SuiteBlahB.TearDownSuite() -- END")
}

func (s *SuiteBlahB) SetUpTest(c *gocheck.C) {
	c.Logf("SuiteBlahB.SetUpTest() -- START")
	c.Logf("SuiteBlahB.SetUpTest() -- END")
}

func (s *SuiteBlahB) TearDownTest(c *gocheck.C) {
	c.Logf("SuiteBlah.TearDownTest() -- START")
	c.Logf("SuiteBlah.TearDownTest() -- END")
}
