package experimental

import (
	gocheck "gopkg.in/check.v1"
)

func (s *SuiteBlahA) TestHelloWorld1(c *gocheck.C) {
	c.Logf("TestHelloWorld1 - really here 1")
}

func (s *SuiteBlahA) TestHelloWorld2(c *gocheck.C) {
	c.Logf("TestHelloWorld2 - really here 2")
}
