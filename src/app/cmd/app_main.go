package main

import (
	"bitbucket.org/claudio_ortega/quadcopter/src/app"
	"os"
)

func main() {
	app.MainAlt(os.Args[:])
}
