package app

import (
	"bitbucket.org/claudio_ortega/quadcopter/src/config"
	"bitbucket.org/claudio_ortega/quadcopter/src/generators"
	"bitbucket.org/claudio_ortega/quadcopter/src/i2cdrivers"
	"bitbucket.org/claudio_ortega/quadcopter/src/restmapper"
	"bitbucket.org/claudio_ortega/quadcopter/src/uarts"
	"bytes"
	"github.com/hashicorp/go-multierror"
	"github.com/ian-kent/go-log/layout"
	"github.com/ian-kent/go-log/levels"
	"github.com/ian-kent/go-log/log"
	"gobot.io/x/gobot/drivers/gpio"
	"gobot.io/x/gobot/drivers/i2c"
	"gobot.io/x/gobot/platforms/raspi"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"runtime/debug"
	"strconv"
	"syscall"
	"time"
)

func MainAlt(args []string) {

	rootLogger := log.Logger()
	rootLogger.Appender().SetLayout(layout.Pattern("[%p][%d] - %l - %m"))
	rootLogger.SetLevel(levels.INFO)
	//rootLogger.SetAppender(appenders.RollingFile("app.log", false))

	err := mainAlt(args)
	if err != nil {
		rootLogger.Error(err)
		os.Exit(1)
	} else {
		os.Exit(0)
	}
}

func mainAlt(args []string) error {

	logger := log.Logger("app")

	lConfig, err := config.CreateInstance(args)
	if err != nil {
		return err
	}

	if lConfig.Help {
		return nil
	}

	logLevel := log.Stol(lConfig.LogLevel)
	if logLevel == 0 {
		logger.Error("ignoring wrong logger level: [%s]", lConfig.LogLevel)
	} else {
		logger.Info("setting logger level at [%s]", lConfig.LogLevel)
		// NOTE: we are setting the root logger in here
		log.Logger().SetLevel(logLevel)
	}

	logger.Info("program starts, exec line is %v", args)

	raspiAdaptor := raspi.NewAdaptor()
	err = raspiAdaptor.Connect()
	if err != nil {
		return err
	}

	logger.Info("raspi adaptor initialized")

	// AD converter
	var ads1x15Driver *i2c.ADS1x15Driver = nil

	if lConfig.AdcBaseAddress != "" {

		dacBaseAddr, err := strconv.ParseInt(lConfig.AdcBaseAddress, 0, 64)
		if err != nil {
			return err
		}

		c := i2c.NewConfig()
		c.WithBus(1)
		c.WithAddress(int(dacBaseAddr))

		ads1x15Driver = i2c.NewADS1015Driver(raspiAdaptor,
			i2c.WithBus(0),
			i2c.WithAddress(int(dacBaseAddr)),
			i2c.WithADS1x15Gain(1),
			i2c.WithADS1x15DataRate(1))

		err = ads1x15Driver.Start()
		if err != nil {
			return err
		}

		defer func() {
			err := ads1x15Driver.Halt()
			if err != nil {
				logger.Error(err)
			}
		}()
	}

	// pcm generator
	var pca9685driver *i2cdrivers.PCA9685Driver = nil

	if lConfig.PwmBaseAddress != "" {

		pwmBaseAddr, err := strconv.ParseInt(lConfig.PwmBaseAddress, 0, 64)
		if err != nil {
			return err
		}

		c := i2c.NewConfig()
		c.WithBus(1)
		c.WithAddress(int(pwmBaseAddr))

		pca9685driver = i2cdrivers.NewPCA9685Driver(raspiAdaptor, c)
		err = pca9685driver.Start()
		if err != nil {
			return err
		}

		defer func() {
			err := pca9685driver.Halt()
			if err != nil {
				logger.Error(err)
			}
		}()
	}

	// clock generator
	var si5351Driver *i2cdrivers.SI5351Driver = nil

	if lConfig.ClockBaseAddress != "" {

		clockBaseAddr, err := strconv.ParseInt(lConfig.ClockBaseAddress, 0, 64)
		if err != nil {
			return err
		}

		c := i2c.NewConfig()
		c.WithBus(1)
		c.WithAddress(int(clockBaseAddr))

		si5351Driver = i2cdrivers.NewSI5351Driver(raspiAdaptor, c, 25000000)
		err = si5351Driver.Start()
		if err != nil {
			return err
		}

		defer func() {
			err := si5351Driver.Halt()
			if err != nil {
				logger.Error(err)
			}
		}()
	}

	// DA converter
	var mcp4725Driver *i2cdrivers.MCP4725Driver = nil

	if lConfig.DacBaseAddress != "" {

		dacBaseAddr, err := strconv.ParseInt(lConfig.DacBaseAddress, 0, 64)
		if err != nil {
			return err
		}

		c := i2c.NewConfig()
		c.WithBus(1)
		c.WithAddress(int(dacBaseAddr))

		mcp4725Driver = i2cdrivers.NewMCP4725Driver(raspiAdaptor, c)
		err = mcp4725Driver.Start()
		if err != nil {
			return err
		}

		defer func() {
			err := mcp4725Driver.Halt()
			if err != nil {
				logger.Error(err)
			}
		}()
	}

	const (
		APP_CLOSE          = "/app-close"
		SET_LOG_LEVEL      = "/set-log-level"
		DISABLE_GC         = "/disable-gc"
		ENABLE_GC          = "/enable-gc"
		FORCE_GC           = "/force-gc"
		LED_ON             = "/led-on"
		LED_OFF            = "/led-off"
		SET_PWM            = "/set-pwm"
		SET_CLOCK          = "/set-clock"
		SET_DAC            = "/set-dac"
		TEST_UART_LOOPBACK = "/test-uart-loopback"
		TEST_UART_GPS      = "/test-uart-gps"
	)

	terminationChan := make(chan bool)
	restRequestChan := make(chan *http.Request, 10)

	if lConfig.RestAddress != "" {

		lRestMapper, err := restmapper.CreateInstance(
			lConfig.RestAddress,
			lConfig.RestPort,
			[]string{
				APP_CLOSE,
				SET_LOG_LEVEL,
				DISABLE_GC,
				ENABLE_GC,
				FORCE_GC,
				LED_ON,
				LED_OFF,
				SET_PWM,
				SET_CLOCK,
				SET_DAC,
				TEST_UART_LOOPBACK,
				TEST_UART_GPS,
			},
			restRequestChan)

		if err != nil {
			return err
		}

		go lRestMapper.Start()

		logger.Info("rest mapper started")
	}

	unixSignalChan := make(chan os.Signal, 1)
	signal.Notify(unixSignalChan, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM)
	uartTicker := time.NewTicker(time.Second)
	pwmTicker := time.NewTicker(100 * time.Millisecond)
	adcTicker := time.NewTicker(time.Second)
	lDACSineGenerator := generators.NewSineGenerator()
	lPWMSineGenerator := generators.NewSineGenerator()
	ledDrivers := make(map[string]*gpio.LedDriver)
	uartMap := make(map[string]*uarts.Uart)
	var pwmGeneratorFunc func() = nil
	var lTxUart *uarts.Uart = nil

	// listen to three input sources:
	//   (1) rest commands,
	//   (2) external unix signals
	//   (3) timer tick
	go func() {

		totalBytesSent := 0
		generatorThreadStarted := false

		for {
			select {

			case unixSignal := <-unixSignalChan:
				logger.Info("received unix signal [%v]", unixSignal)
				terminationChan <- true

			case <-uartTicker.C:
				if lTxUart != nil {
					bytesSent, errWrite := lTxUart.Write(bytes.Repeat([]byte{byte(0x55)}, 1000))
					if errWrite != nil {
						logger.Error(errWrite)
					} else {
						totalBytesSent += bytesSent
						logger.Debug("sent:[%d], total:[%d]", bytesSent, totalBytesSent)
					}
				}

			case <-pwmTicker.C:
				if pwmGeneratorFunc != nil {
					pwmGeneratorFunc()
				}

			case <-adcTicker.C:
				if ads1x15Driver != nil {
					value, err := ads1x15Driver.AnalogRead("0")
					if err != nil {
						logger.Error(err)
					}
					logger.Debug("value:[%d]", value)
				}

			case r := <-restRequestChan:

				logger.Info("rest command received: [%v]", r.URL)

				switch r.URL.Path {

				case SET_LOG_LEVEL:
					logLevelAsString, found := r.Form["level"]
					if found {
						logLevel := log.Stol(logLevelAsString[0])
						if logLevel != 0 {
							logger.Info("setting logger level at [%s]", levels.LogLevelsToString[logLevel])
							log.Logger().SetLevel(logLevel)
						} else {
							logger.Warn("setting logger level failed for [%s]", logLevelAsString[0])
						}
					} else {
						logger.Warn("setting logger level failed for [%v]", r.URL)
					}

				case ENABLE_GC:
					logger.Info("enabling gc")
					debug.SetGCPercent(100)

				case DISABLE_GC:
					logger.Info("disabling gc")
					debug.SetGCPercent(-1)

				case FORCE_GC:
					logger.Info("forcing gc")
					runtime.GC()

				case TEST_UART_LOOPBACK:

					serialPortNameAsString, foundN := r.Form["name"]
					baudRateAsString, foundBR := r.Form["baudrate"]

					if foundN && foundBR {
						lUart, ok := uartMap[serialPortNameAsString[0]]
						if !ok {
							baudrate, _ := strconv.ParseInt(baudRateAsString[0], 0, 0)
							lUart, err = uarts.CreateInstance(serialPortNameAsString[0], int(baudrate))
							if err != nil {
								logger.Warn("failed to create serial on %d", serialPortNameAsString[0])
							} else {
								uartMap[serialPortNameAsString[0]] = lUart
								lTxUart = lUart
								logger.Info("uart initialized and testing loopback in its own thread")
								go lUart.ConsumeAndLogForLoopBack()
							}
						}
					} else {
						logger.Info("failed to get data from rest API")
					}

				case TEST_UART_GPS:

					serialPortNameAsString, foundN := r.Form["name"]
					baudRateAsString, foundBR := r.Form["baudrate"]

					if foundN && foundBR {
						lUart, ok := uartMap[serialPortNameAsString[0]]
						if !ok {
							baudrate, _ := strconv.ParseInt(baudRateAsString[0], 0, 0)
							lUart, err = uarts.CreateInstance(serialPortNameAsString[0], int(baudrate))
							if err != nil {
								logger.Warn("failed to create serial on %d", serialPortNameAsString[0])
							} else {
								uartMap[serialPortNameAsString[0]] = lUart
								logger.Info("uart initialized and listening for input on %s", serialPortNameAsString[0])
								go lUart.ConsumeAndLogForGPS()
							}
						}
					} else {
						logger.Info("failed to get data from rest API")
					}

				case APP_CLOSE:
					logger.Info("close received, shutting down")
					terminationChan <- true

				case LED_ON:

					pinAsString, found := r.Form["pin"]

					if found {
						ledDriver, found := ledDrivers[pinAsString[0]]

						if !found {
							ledDriver = gpio.NewLedDriver(raspiAdaptor, pinAsString[0])
							err := ledDriver.Start()
							if err != nil {
								logger.Error(err)
							} else {
								ledDrivers[pinAsString[0]] = ledDriver
							}
						}

						err := ledDriver.On()
						if err != nil {
							logger.Error(err)
						}
					}

				case LED_OFF:

					pinAsString, found := r.Form["pin"]

					if found {
						ledDriver, driverFound := ledDrivers[pinAsString[0]]
						if driverFound {
							err := ledDriver.Off()
							if err != nil {
								logger.Error(err)
							}
						}
					}

				case SET_PWM:
					if pca9685driver != nil {

						pulseFreqAsString, foundPulseFreq := r.Form["pulsefreq"]

						channelAsString, foundChannel := r.Form["channel"]
						minWidthAsString, foundMinWidth := r.Form["minwidth"]
						maxWidthAsString, foundMaxWidth := r.Form["maxwidth"]
						modulationPeriodAsString, foundModulationPeriod := r.Form["modperiod"]

						var result *multierror.Error

						if foundPulseFreq {
							freq, _ := strconv.ParseFloat(pulseFreqAsString[0], 32)
							err := pca9685driver.SetFrequency(float32(freq))
							result = multierror.Append(result, err)

						} else if foundChannel && foundMinWidth && foundMaxWidth && foundModulationPeriod {

							channel, _ := strconv.ParseInt(channelAsString[0], 0, 0)
							minWidthCount, _ := strconv.ParseInt(minWidthAsString[0], 0, 0)
							maxWidthCount, _ := strconv.ParseInt(maxWidthAsString[0], 0, 0)
							modulationFreqPeriod, _ := strconv.ParseInt(modulationPeriodAsString[0], 0, 0)

							err := pca9685driver.SetPulseWidth(int(channel), uint16(0))

							if err == nil {
								lPWMSineGenerator = generators.NewSineGenerator()

								lPWMSineGenerator.Init(
									int(modulationFreqPeriod),
									int(minWidthCount),
									int(maxWidthCount))

								pwmGeneratorFunc = func() {
									_, nextWidth := lPWMSineGenerator.NextSample()
									err := pca9685driver.SetPulseWidth(int(channel), uint16(nextWidth))
									if err != nil {
										logger.Error(err)
									} else {
										logger.Info("pwmGeneratorFunc() -- channel:%d, nextWidth:%d", channel, nextWidth)
									}
								}
							} else {
								result = multierror.Append(result, err)
							}

						} else {
							logger.Info("provided REST args are incomplete ")
						}

						if result.ErrorOrNil() != nil {
							logger.Error(result.ErrorOrNil())
						}
					}

				case SET_CLOCK:
					if si5351Driver != nil {

						channelAsString, foundChannel := r.Form["channel"]
						freqString, foundFreq := r.Form["freq"]

						if foundFreq && foundChannel {
							freq, _ := strconv.ParseFloat(freqString[0], 32)
							channel, _ := strconv.ParseInt(channelAsString[0], 0, 0)
							err := si5351Driver.SetFrequency(int(channel), int(freq))
							if err != nil {
								logger.Error(err)
							}
						}
					}

				case SET_DAC:
					if mcp4725Driver != nil {

						valueString, foundValue := r.Form["value"]

						if foundValue {
							value, _ := strconv.ParseInt(valueString[0], 10, 64)
							err := mcp4725Driver.SetValue(int(value))
							if err != nil {
								logger.Error(err)
							}
						}

						periodString, foundPeriod := r.Form["period"]

						if foundPeriod {

							period, _ := strconv.ParseInt(periodString[0], 10, 64)
							lDACSineGenerator.Init(int(period), 0, 4095)

							if !generatorThreadStarted {

								generatorThreadStarted = true

								go func() {
								loopOut:
									for {
										nextIndex, nextSample := lDACSineGenerator.NextSample()
										if nextIndex%20000 == 0 {
											logger.Info("nextIndex:%d", nextIndex)
											time.Sleep(time.Millisecond * 100)
										}
										err := mcp4725Driver.SetValue(nextSample)
										if err != nil {
											break loopOut
										}
									}
								}()
							}
						}
					}
				}
			}
		}
	}()

	logger.Info("waiting for app termination..")
	<-terminationChan

	for _, driver := range ledDrivers {

		err := driver.Off()
		if err != nil {
			logger.Error(err)
		}

		err = driver.Halt()
		if err != nil {
			logger.Error(err)
		}
	}

	for _, lUart := range uartMap {
		err = lUart.Close()
		if err != nil {
			logger.Error(err)
		}
	}

	logger.Info("app terminated.")

	return nil
}
