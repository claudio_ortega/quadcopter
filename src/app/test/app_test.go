package app_test

import (
	"bitbucket.org/claudio_ortega/quadcopter/src/app"
	"github.com/hashicorp/go-multierror"
	"github.com/ian-kent/go-log/layout"
	"github.com/ian-kent/go-log/log"
	"github.com/pkg/errors"
	"testing"
)

func TestHelp(t *testing.T) {
	app.MainAlt([]string{
		"app",
		"-help",
	})
}

func TestUSB0(t *testing.T) {
	app.MainAlt([]string{
		"app",
		"-serial-port", "/dev/ttyUSB0",
		"-log-level", "debug",
		"-msg-size", "100",
	})
}

func TestUSB1(t *testing.T) {
	app.MainAlt([]string{
		"app",
		"-serial-port", "/dev/ttyUSB1",
		"-log-level", "debug",
		"-msg-size", "100",
	})
}

func TestAMA0(t *testing.T) {
	app.MainAlt([]string{
		"app",
		"-serial-port=/dev/ttyAMA0",
		"-log-level=debug",
		"-msg-size=100",
	})
}

func TestRest(t *testing.T) {
	app.MainAlt([]string{
		"app",
		"-rest-server=localhost",
	})
}

func TestDisabled(t *testing.T) {
	app.MainAlt([]string{
		"app",
		"-log-level=debug",
	})
}

func TestPwm(t *testing.T) {
	app.MainAlt([]string{
		"app",
		"-pwm-base=0x40",
		"-log-level=debug",
	})
}

func Test00(t *testing.T) {
	var result *multierror.Error = nil
	t.Logf("error(1):%v", result)
	err := errors.New("1")
	result = multierror.Append(result, err)
	t.Logf("error(2):%v", result)
	err1 := errors.New("2")
	result = multierror.Append(result, err1)
	t.Logf("error(3):%v", result)
	result = multierror.Append(result, nil)
	t.Logf("error(4):%v", result)
	t.Logf("error(5):%v", result.Errors)
	t.Logf("error(6):%v", result.ErrorOrNil())
}

// [INFO][2018-12-24 20:15:57.437171000 -0800 PST] - test/log_test.go:19 - Hello Ian
//  to make this work we had to change inside
//  go-paths/poplar/src/github.com/ian-kent/go-log/layout/pattern.go
//    -runtime.Caller(2)
//    +runtime.Caller(6)
func DemoGoLogFix() {
	log.Logger().Appender().SetLayout(layout.Pattern("[%p][%d] - %l - %m"))
	loggerA := log.Logger("blah.uart")
	loggerA.Printf("Hello Ian")
}
