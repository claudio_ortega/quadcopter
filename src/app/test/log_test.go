package app_test

import (
	"github.com/ian-kent/go-log/layout"
	"github.com/ian-kent/go-log/log"
	"testing"
)

// [INFO][2018-12-24 20:15:57.437171000 -0800 PST] - test/log_test.go:19 - Hello Ian
//  to make this work change go-paths/poplar/src/github.com/ian-kent/go-log/layout/pattern.go
//  -runtime.Caller(2)
//  +runtime.Caller(6)
func TestFlags(t *testing.T) {

	// do this just once in some config .go file
	log.Logger().Appender().SetLayout(layout.Pattern("[%p][%d] - %l - %m"))

	// repeat and rinse in each diferent .go file..
	loggerA := log.Logger("blah.blah2")
	loggerA.Printf("Hello there")
}
