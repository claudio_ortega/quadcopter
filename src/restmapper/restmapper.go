package restmapper

import (
	"fmt"
	"github.com/hashicorp/go-multierror"
	"github.com/ian-kent/go-log/log"
	"github.com/ian-kent/go-log/logger"
	"net/http"
)

type RestMapper struct {
	log             logger.Logger
	serverAddress   string
	serverPort      int
	terminationChan chan bool
}

func CreateInstance(
	inServerAddress string,
	inServerPort int,
	restKeys []string,
	httpRequestChan chan *http.Request) (*RestMapper, error) {

	if inServerAddress == "" {

		return nil, fmt.Errorf("the name for the server is empty")

	} else {

		tmpLog := log.Logger("restmapper")

		// httpResponder component for HTTP requests
		httpResponder := func(w http.ResponseWriter, r *http.Request) *multierror.Error {

			var result *multierror.Error

			_, err := fmt.Fprintf(w, "%s %s %s %s\n", r.Method, r.URL.Path, r.URL, r.Proto)
			result = multierror.Append(result, err)

			for k, v := range r.Header {
				_, err := fmt.Fprintf(w, "Header[%q] = %q\n", k, v)
				result = multierror.Append(result, err)
			}

			_, err = fmt.Fprintf(w, "Host = %q\n", r.Host)
			result = multierror.Append(result, err)

			_, err = fmt.Fprintf(w, "RemoteAddr = %q\n", r.RemoteAddr)
			result = multierror.Append(result, err)

			err = r.ParseForm()
			result = multierror.Append(result, err)

			for k, v := range r.Form {
				_, err = fmt.Fprintf(w, "Form[%q] = %q\n", k, v)
				result = multierror.Append(result, err)
			}

			return result
		}

		for _, key := range restKeys {
			http.HandleFunc(
				key,
				func(w http.ResponseWriter, r *http.Request) {
					err := httpResponder(w, r)
					if err.ErrorOrNil() != nil {
						tmpLog.Error("error:%v", err.Errors)
					}
					httpRequestChan <- r
				})
		}

		return &RestMapper{
				log:           tmpLog,
				serverAddress: inServerAddress,
				serverPort:    inServerPort,
			},
			nil
	}
}

func (r *RestMapper) Start() {
	serverId := fmt.Sprintf("%s:%d", r.serverAddress, r.serverPort)

	r.log.Info("http server is starting on %s", serverId)

	err := http.ListenAndServe(serverId, nil)

	if err != nil {
		r.log.Error(err)
	} else {
		r.log.Info("http server is stopping on %s", serverId)
	}
}
