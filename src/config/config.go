package config

import (
	"flag"
	"github.com/pkg/errors"
)

type Config struct {
	Help             bool
	LogLevel         string
	RestPort         int
	RestAddress      string
	PwmBaseAddress   string
	ClockBaseAddress string
	DacBaseAddress   string
	AdcBaseAddress   string
}

func CreateInstance(args []string) (*Config, error) {

	commandLine := flag.NewFlagSet(args[0], flag.ContinueOnError)

	flagHelp := commandLine.Bool(
		"help",
		false,
		"print out this help and quit")

	flagLogLevelAsString := commandLine.String(
		"log-level",
		"info",
		"logging level")

	flagRestPort := commandLine.Int(
		"rest-port",
		9000,
		"rest port number, anything above 1024.")

	flagRestServer := commandLine.String(
		"rest-server",
		"",
		"rest server name, disabled by default")

	flagClockBaseAddr := commandLine.String(
		"clock-base",
		"",
		"I2C base address for the SI5351 clock generator chip, disabled by default")

	flagPwmBaseAddr := commandLine.String(
		"pwm-base",
		"",
		"I2C base address for the PCA9685 pwm chip, disabled by default")

	flagDacBaseAddr := commandLine.String(
		"dac-base",
		"",
		"I2C base address for the MCP4725 dac chip, disabled by default")

	flagAcdBaseAddr := commandLine.String(
		"adc-base",
		"",
		"I2C base address for the ADS1x15 adc chip, disabled by default")

	err := commandLine.Parse(args[1:])
	if err != nil {
		return nil, errors.Errorf("error parsing command line: [%v]", err)
	}

	if *flagHelp {
		commandLine.Usage()
	}

	return &Config{
			Help:             *flagHelp,
			LogLevel:         *flagLogLevelAsString,
			RestPort:         *flagRestPort,
			RestAddress:      *flagRestServer,
			PwmBaseAddress:   *flagPwmBaseAddr,
			ClockBaseAddress: *flagClockBaseAddr,
			DacBaseAddress:   *flagDacBaseAddr,
			AdcBaseAddress:   *flagAcdBaseAddr,
		},
		nil
}
