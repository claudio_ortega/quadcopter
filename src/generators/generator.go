package generators

import (
	"math"
)

func NewSineGenerator() *SineGenerator {
	return &SineGenerator{}
}

type SineGenerator struct {
	samples           []int
	nextIndexCircular uint16
	countProduced     uint64
}

func (g *SineGenerator) Init(length int, minValue int, maxValue int) {

	g.samples = make([]int, length, length)

	g.nextIndexCircular = 0

	for i := range g.samples {
		alfa := 2.0 * math.Pi * float64(i) / float64(length)
		g.samples[i] = minValue + int((1.0+math.Sin(alfa))*float64((maxValue-minValue)/2))
	}
}

func (g *SineGenerator) NextSample() (count uint64, sample int) {
	g.countProduced++
	nextIndex := g.nextIndexCircular
	nextSample := g.samples[nextIndex]
	g.nextIndexCircular = (nextIndex + 1) % uint16(len(g.samples))
	return g.countProduced, nextSample
}
