package generators_test

import (
	"bitbucket.org/claudio_ortega/quadcopter/src/generators"
	"github.com/ian-kent/go-log/log"
	"testing"
)

func Test00(t *testing.T) {

	logger := log.Logger("generator_test")

	nSamples := 8

	lGenerator := generators.NewSineGenerator()
	lGenerator.Init(nSamples, 0, 4095)

	for i := 0; i < nSamples; i++ {
		_, next := lGenerator.NextSample()
		logger.Printf("%d:%d", i, next)
	}
}
