logging (logrus, changed to go-log) - DONE

tests (gocheck) - DONE

command line options package - DONE

linux signals - DONE

web server for commands - DONE

dependencies check (dep) - SUSPENDED

change on log level - DONE

debug in r-pi by using delve + goland SUSPENDED (delve does not work on 32 bits OS, as raspbian)

GPIO output - DONE

PWM control (PCA9685) - DONE

general purpose programmable clock (SI5351) - DONE

Digital to analog (PCM4725), to test garbage collection pauses - DONE

UART - tested on ttyUSB0 with 5 kbytes/per second (>40kbits/sec)
./uart -log-level debug -msg-size 5000 -serial-port /dev/ttyUSB0
used library github.com/tarm/serial --- DONE

UART (on AMA0), needed to test from GPS @ 9600 N81 (Nmea) - DONE

S-bus @ 100kbaud

either
  try to set 100Kbaud to any UART - NEXT  - DEAD END - not possible to set @ 100kbaud, dead end !!!
or
  get Analog to digital (ADS1015) working, needed to decode PWM - NEXT


magnetometer on I2C - PENDING

real time kernel - PENDING
google: "preempt scheduling in raspbian"
http://www.frank-durr.de/?p=203
https://raspberrypi.stackexchange.com/questions/8970/default-kernel-preemption-vs-real-time-patch


MAVLink - PENDING
https://en.wikipedia.org/wiki/MAVLink
https://diydrones.com/forum/topics/mavlink-tutorial-for-absolute-dummies-part-i?groupUrl=arducopterusergroup&groupId=705844%3AGroup%3A394475&id=705844%3ATopic%3A1472930&page=1#comments
https://mavlink.io/en
https://mavlink.io/en


compare EMBD with gobot in terms of performance
writing into DAC chip.


---------------------------------------------------------------------
IMUs
https://learn.adafruit.com/comparing-gyroscope-datasheets/overview
MPU-9250
LSM9DS1
BMI055
https://www.mouser.com/ds/2/783/BMI055_Shuttleboardflyer_BST_20170111-1221243.pdf

MPU9250 gyro/acc/mag (all in one)
https://makersportal.com/blog/2019/11/11/raspberry-pi-python-accelerometer-gyroscope-magnetometer

BME280 pressure
https://www.amazon.com/gp/product/B01N47LZ4P/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1


Magnetometer read/write/calibrate
Youtube MicWro Engr channel
https://www.youtube.com/watch?v=MinV5V1ioWg


---------------------------------------------------------------------
PWM controller,
PCA9685
PWM chip breakout for PCA9685
http://wiki.sunfounder.cc/index.php?title=PCA9685_16_Channel_12_Bit_PWM_Servo_Driver
https://cdn-shop.adafruit.com/datasheets/PCA9685.pdf
breakout PCB and schematic
https://learn.adafruit.com/16-channel-pwm-servo-driver/downloads

---------------------------------------------------------------------
UART
lookup SC16IS750 I2C/SPI to UART Bridge Module
https://sandboxelectronics.com/?p=472
https://www.sparkfun.com/products/retired/9981

Amazon
ZYAMY A61 CP2102 Module USB to TTL USB 2.0 Serial Module UART

Qunqi 2pcs 3.3V 5.5V FT232RL FTDI USB to TTL Serial Adapter Module for Arduino Mini Port

---------------------------------------------------------------------
golang-UI
https://github.com/avelino/awesome-go#gui

only for GUI inspiration
look at cayenne mydevices.com

Golang UI
https://github.com/divanvisagie/json-translator

Maybe good
https://github.com/fyne-io/fyne

Maybe good
https://www.linkedin.com/pulse/using-go-language-build-stand-alone-gui-app-peter-howard/

Maybe good
https://github.com/gotk3/gotk3

---------------------------------------------------------------------
DAC
MCP4725 Breakout Board - 12-Bit DAC w/I2C Interface
https://www.adafruit.com/product/935

ADC
ADS1015 12-BIT ADC - 4 CHANNEL WITH PROGRAMMABLE GAIN AMPLIFIER
https://www.adafruit.com/product/1083




