
FrSky Taranis and X8R Receiver Binding
https://www.youtube.com/watch?v=VJLMfJYXoOY&t=11s

How to Bind the Frsky X8R to the Taranis Radio
https://www.youtube.com/watch?v=1HOSKQUbbZw&t=49s

FrSky Taranis and X8R Receiver SBUS Setup w/ NAZA Flight Controller
(SBUS mode)
https://www.youtube.com/watch?v=kl5wuMiZ1_w

FrSky Taranis 3 Flight Modes and Forced Failsafe Override with DJI NAZA on QAV400 Quadcopter
(no SBUS mode, but important because the trick of the mixers for channel 7)
https://www.youtube.com/watch?v=-gA5o_g4oW4

Taranis X8R Naza M
https://www.youtube.com/watch?v=CExo5Mvdd3k

DJI Naza-M V2 Assistant Software Introduction
https://www.youtube.com/watch?v=tUAXL575RpU

How to setup 16 channels on FrSky X8R using SBUS
https://www.youtube.com/watch?v=jUkutJrzXFI&t=309s

Setting up Telemetry and Alerts on Taranis X8R
https://www.youtube.com/watch?v=rLMpBZZSMno

Naza-M Control Mode Switch
http://wiki.dji.com/en/index.php/Naza-M_Control_Mode_Switch

Taranis Setup Tutorial
https://www.youtube.com/watch?v=f40mXxzU4hY

Devo F12e RX1202 on NAZA M V2 - Part 1 - wiring, binding, motors
https://www.youtube.com/watch?v=B3IauShClBs&t=119s

