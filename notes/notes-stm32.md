https://www.youtube.com/watch?time_continue=32&v=ViwcxS9Rl-Y&feature=emb_logo


https://www.st.com/content/st_com/en/support/learning/stm32-education/text-books.html
https://legacy.cs.indiana.edu/~geobrown/book.pdf

Good book:
https://leanpub.com/mastering-stm32?utm_campaign=mastering-stm32&utm_medium=embed&utm_source=embedder
https://github.com/cnoviello/mastering-stm32
https://www.carminenoviello.com/2014/12/28/setting-gcceclipse-toolchain-stm32nucleo-part-1/




From ST
https://www.st.com/en/development-tools/sw4stm32.html

comparison table
http://ww1.microchip.com/downloads/en/DeviceDoc/60001455D.pdf

http://www.brokking.net/ymfc-32_main.html
https://os.mbed.com/built-with-mbed/


https://www.youtube.com/watch?v=rLDqQ2L_mUQ
https://www.youtube.com/watch?v=t5phi3nT8OU

https://www.youtube.com/watch?v=zOByx3Izf5U
https://github.com/pms67/HadesFCS
https://www.youtube.com/watch?v=hGcGPUqB67Q

https://www.youtube.com/watch?v=vPGChdmfKl0


Notes on using CLion as a dev platform for STM32


Sources:
(1)
    https://plugins.jetbrains.com/plugin/11284-openocd--esp32-support-for-embedded-development
(2)
    https://github.com/cnoviello/mastering-stm32


Debugging pod
https://www.st.com/en/development-tools/st-link-v2.html
https://smile.amazon.com/ST-LINK-V2-circuit-debugger-programmer/dp/B00SDTEM1I/ref=sr_1_3?dchild=1&keywords=ST-LINK%2FV2&qid=1624155021&sr=8-3

***
stm32duino
https://www.arduinolibraries.info/authors/stm32duino
https://github.com/stm32duino

nucleo board 64 --- F446RE
    https://docs.zephyrproject.org/latest/boards/arm/nucleo_f411re/doc/index.html

nucleo UART tutorial
    https://www.dmcinfo.com/latest-thinking/blog/id/9372/nucleo-uart-tutorial
    https://github.com/stm32duino/wiki/wiki/API#hardwareserial
