2019/4/27 - Bruno's Alambique


-- Config GPIO

gpio -g mode  bcm-index in/out 
gpio -g write bcm-index 1/0
ie:
gpio -g mode  17 out
gpio -g write 17 1


--- Config 1-Wire measurement with DS18B20 ----
sudo dtoverlay w1-gpio gpiopin=4 pullup=0
sudo modprobe w1-gpio
sudo modprobe w1-therm

cat /sys/bus/w1/devices/28-01143c50cbaa/w1_slave
cat /sys/bus/w1/devices/28-01143f922baa/w1_slave


https://pinout.xyz/pinout/1_wire

https://www.youtube.com/watch?v=upIx78DRzao
	https://www.maximintegrated.com/en/app-notes/index.mvp/id/1796
	https://www.maximintegrated.com/en/app-notes/index.mvp/id/148

*** https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/temperature/
https://www.modmypi.com/blog/ds18b20-one-wire-digital-temperature-sensor-and-the-raspberry-pi



--- temp measurement with python

python<enter>

tfile = open("/sys/bus/w1/devices/28-021692450b8a/w1_slave") 
text = tfile.read() 
tfile.close() 

# Split the text with new lines (\n) and select the second line. 
secondline = text.split("\n")[1] 

# Split the line into words, referring to the spaces, and select the 10th word (counting from 0). 
temperaturedata = secondline.split(" ")[9] 

# The first two characters are "t=", so get rid of those and convert the temperature from a string to a number. 
temperature = float(temperaturedata[2:]) 

# Put the decimal point in the right place and display it. 
temperature = temperature / 1000 
print temperature

---- temp control
import RPi.GPIO as GPIO 
GPIO.setmode(GPIO.BCM) 
GPIO.setup(17, GPIO.OUT) 
GPIO.output(17, GPIO.HIGH) 
GPIO.output(17, GPIO.LOW)


MQTT
https://www.youtube.com/watch?v=bpg6RSHS4Zc

NodeRed + InfluxDB + Grafana
https://www.youtube.com/watch?v=JdV4x925au0&t=7s

NodeRed
https://nodered.org/
https://nodered.org/docs/hardware/raspberrypi
https://flows.nodered.org/node/node-red-dashboard
https://www.youtube.com/watch?v=YahFRqf-rFA
https://www.youtube.com/watch?v=WxUTYzxIDns
best 5 nodes:
https://www.youtube.com/watch?v=ccKAsXGTELc

NodeRed Dashboard
https://www.youtube.com/watch?v=X8ustpkAJ-U

Butlini
https://www.youtube.com/watch?v=GeN7g4bdHiM

JS
https://www.w3schools.com/js/js_date_methods.asp


---- solid state relay 110v 40A
https://smile.amazon.com/dp/B01HCFJC0Y/ref=sspa_dk_detail_7?psc=1
https://www.sparkfun.com/products/13015
https://smile.amazon.com/s?k=solid+state+relay+110v&ref=nb_sb_noss



-- from the internet
garage-poplar.ddns.net:1880/ui


pressure measurement
--------------------

-- requirement: measure -70mBars under sea level
Sea level pressure is 14.5 PSI
1PSI = 70mBars
1mBar = 100 pascal

-- pressure sensors

(1a)
SKU237545 - analog output  (0 - 1.2MPa)
https://www.youtube.com/watch?v=AB7zgnfkEi4
(10$ aliexpress.com)

(1b)
MPXV6115V  analog output (0-400kPa)
https://www.mouser.com/datasheet/2/302/MPXV6115V-1127049.pdf
(20$ mouser.com) 

(1a/1b)
AD converter (for SKU237545) I2C
adafruit ADS1015 (10$)
https://www.adafruit.com/product/1083
(adafruit 10$)

(2)
adafruit MPRLS 0-25 PSI (Honeywell MPR series)  I2C
https://www.adafruit.com/product/3965
https://sensing.honeywell.com/honeywell-sensing-micropressure-board-mount-pressure-mpr-series-datasheet-32332628-d-en.pdf
(adafruit 15$)




