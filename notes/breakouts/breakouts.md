

MPU-9250 breakout from amazon with schematic
https://smile.amazon.com/HiLetgo-Gyroscope-Acceleration-Accelerator-Magnetometer/dp/B01I1J0Z7Y/ref=zg_bs_306924011_4?_encoding=UTF8&psc=1&refRID=CJ5C59X0PSTHZV7SA6G2

MPU-9250 datasheets
https://cdn.sparkfun.com/assets/learn_tutorials/5/5/0/MPU-9250-Register-Map.pdf
https://invensense.tdk.com/wp-content/uploads/2015/02/PS-MPU-9250A-01-v1.1.pdf

Adafruit LSM6DSOX + LIS3MDL - Precision 9 DoF IMU - STEMMA QT / Qwiic PID: 4517 
Adafruit LIS3MDL - Magnetometer 3DF

Lidar LUNA
https://github.com/budryerson/TFLuna-I2C

